#include <ros/ros.h>

#include <assembly_control_ros/assembly_station_state.h>
#include <assembly_control_ros/assembly_station_command.h>
#include <assembly_control_ros/assembly_station_input.h>


#include <common/machine_controller.hpp>

class AssemblyStation
    : public MachineController<assembly_control_ros::assembly_station_state,
                               assembly_control_ros::assembly_station_input,
                               assembly_control_ros::assembly_station_command,
                               assembly_control_ros::assembly_station_input> {
public:
    AssemblyStation(ros::NodeHandle node)
        : MachineController(node, "assembly_station"), state_(State::Off) {
    }

    virtual void process() override {
        assembly_control_ros::assembly_station_command commands;
        assembly_control_ros::assembly_station_input inputs;

        auto& input = getInputs();

        switch (state_) {
        case State::Off:
            if (input.AS_start) {
                input.AS_start = false;
                ROS_INFO("[assemblystation] check");
                state_ = State::check;
            }
            break;

        case State::check:
                commands.check = true;
	            sendCommands(commands);
                if(getState().valid){
                    ROS_INFO("[assemblystation] On");
                    state_ = State::On;
                }
                break;

        case State::On:
                
            if (getState().evacuated) {
                ROS_INFO("[assemblystation] Off");
                state_ = State::Off;
            }
            break;
        }

        sendCommands(commands);
    }

private:
    enum class State { On, Off, check };

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "assembly_station");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    AssemblyStation station(node);

    while (ros::ok()) {
        station.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}
