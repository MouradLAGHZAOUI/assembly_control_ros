#include <ros/ros.h>

#include <assembly_control_ros/controller_input.h>
#include <assembly_control_ros/controller_output.h>

#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>

#include <assembly_control_ros/memoire_input.h>
#include <assembly_control_ros/memoire_output.h>

#include <assembly_control_ros/supply_conveyor_input.h>
#include <assembly_control_ros/supply_conveyor_output.h>

#include <assembly_control_ros/evacuation_conveyor_input.h>
#include <assembly_control_ros/evacuation_conveyor_output.h>


#include <assembly_control_ros/controller_input.h>
#include <assembly_control_ros/controller_output.h>

#include <assembly_control_ros/assembly_station_input.h>
#include <assembly_control_ros/assembly_station_output.h>


#include <assembly_control_ros/camera_input.h>
#include <assembly_control_ros/camera_output.h>


#include <common/M_controller.hpp>

class Controller
    : public MController<assembly_control_ros::supply_conveyor_input,
                                assembly_control_ros::supply_conveyor_output,
	                            assembly_control_ros::camera_input,
                                assembly_control_ros::camera_output,
			                    assembly_control_ros::robot_input,
                                assembly_control_ros::robot_output,
                                assembly_control_ros::controller_input,
                                assembly_control_ros::controller_output,
                                assembly_control_ros::evacuation_conveyor_input,
                                assembly_control_ros::evacuation_conveyor_output,
			                    assembly_control_ros::assembly_station_input,
                                assembly_control_ros::assembly_station_output,
			                    assembly_control_ros::memoire_input,
                                assembly_control_ros::memoire_output> {
public:
    Controller(ros::NodeHandle node)
        : MController(node, "controller"), state_(State::wait) {
    }

    virtual void process() override {

        assembly_control_ros::controller_output outputs;
        assembly_control_ros::controller_input inputs;

        assembly_control_ros::robot_input outputsROB;
        assembly_control_ros::camera_input outputsCAM;
        assembly_control_ros::memoire_input outputsMEM;
        assembly_control_ros::assembly_station_input outputsASS;
        assembly_control_ros::evacuation_conveyor_input outputsEVA;
        assembly_control_ros::supply_conveyor_input outputsSUP;


        auto& inputsSUP = getInputs_supply_conveyor();
        auto& inputsCAM = getInputs_camera();
        auto& inputsROB = getInputs_robot();
        auto& inputsCON = getInputs_controller();
        auto& inputsEVA = getInputs_evacuation_conveyor();
        auto& inputsASS = getInputs_assembly_station();
        auto& inputsMEM = getInputs_memoire();


        

      switch (state_) {
        case State::wait:

                ROS_INFO("[Controller] wait"); 
                ros::Duration(5.0).sleep();
                outputsROB.io_move_right = true;
                sendOuputs_robot(outputsROB);
                state_ = State::wait_at_sc;
            break;
            
            
            
        case State::wait_at_sc:

            if(inputsSUP.part_available ){
                ROS_INFO("[Controller] start camera");
                inputsSUP.part_available = false;
                outputsCAM.start_recognition = true;
		        sendOuputs_camera(outputsCAM);
                state_ = State::wait_cam;
            }
            break;

        case State::wait_cam:

            if(inputsROB.io_at_supply_conveyor && inputsCAM.part_analyzed){
                ROS_INFO("[Controller] start grasp");
                inputsROB.io_at_supply_conveyor = false;
                inputsCAM.part_analyzed = false;
                outputsROB.io_grasp = true;
		        sendOuputs_robot(outputsROB);
                state_ = State::wait_grasp;
            }
            break;


        case State::wait_grasp:
            if(inputsROB.io_part_grasped ){
                ROS_INFO("[Controller] grasped");
                inputsROB.io_part_grasped = false;
                outputsSUP.restart = true;
		        sendOuputs_supply_conveyor(outputsSUP);
                state_ = State::decision;
            }
            break;

        

        case State::decision:
        ROS_INFO("[Controller] decision");
            if(inputsCAM.go_ass){
                ROS_INFO("[Controller] Go_assembly_station");
                inputsCAM.go_ass = false;
                outputsROB.io_move_left = true;
		        sendOuputs_robot(outputsROB);

                state_ = State::wait_at_as;
            }

            if(inputsCAM.go_evac){
                ROS_INFO("[Controller] Go_evacuation_convoyer");
                inputsCAM.go_evac = false;
                outputsROB.io_move_right = true;
                sendOuputs_robot(outputsROB);
                outputsEVA.stop_EC = true;
		        sendOuputs_evacuation_conveyor(outputsEVA);

                state_ = State::wait_at_ev;

                

            }

        break;


        case State::wait_at_ev:
        
            if(inputsROB.io_at_evacuation_conveyor){
                inputsROB.io_at_evacuation_conveyor = false;
                outputsROB.io_release = true;
		        sendOuputs_robot(outputsROB);
                state_ = State::rels; 
            }
            break;

        case State::rels:
            if(inputsROB.io_part_released){
                outputsROB.io_move_left = true;
                sendOuputs_robot(outputsROB);
                outputsEVA.start_EC = true;
		        sendOuputs_evacuation_conveyor(outputsEVA);
                state_ = State::wait_at_sc;
            }
            break;

        case State::wait_at_as:

            if(inputsROB.io_at_assembly_station ){
                inputsROB.io_at_assembly_station = false; 
                
                if(inputsCAM.do_part1){
                    ROS_INFO("[Controller] Assembly_part1");
                    inputsCAM.do_part1 = false;
                    outputsROB.io_assemble_part1 = true;
				    sendOuputs_robot(outputsROB);
                    state_ = State::ass_part1;
                }
                if(inputsCAM.do_part2){
                    ROS_INFO("[Controller] Assembly_part2");
                    inputsCAM.do_part2 = false;
                    outputsROB.io_assemble_part2 = true;
				    sendOuputs_robot(outputsROB);
                    state_ = State::ass_part2;
                }
                if(inputsCAM.do_part3){
                    ROS_INFO("[Controller] Assembly_part3");
                    inputsCAM.do_part3 = false;
                    outputsROB.io_assemble_part3 = true;
				    sendOuputs_robot(outputsROB);
                    state_ = State::ass_part3;
                }
            break;

        case State::ass_part1:
                        
            if(inputsROB.io_part1_assembled ){
                inputsROB.io_part1_assembled = false;
                cmpt2 ++;
                
                if(cmpt2 == 3){
                    ROS_INFO("[Controller] 3 parts");
                    state_ = State::assemblage;
                }
                else
                {
                    state_ = State::wait;
                }
                                    
            }
            break;

        case State::ass_part2:
                            
            if(inputsROB.io_part2_assembled ){
                inputsROB.io_part2_assembled = false;
                //state_ = State::wait;
                cmpt2 ++;
                
                if(cmpt2 == 3){
                    ROS_INFO("[Controller] 3 parts");
                    outputsASS.AS_start = true;
					sendOuputs_assembly_station(outputsASS);
                    state_ = State::assemblage;
                }
                else
                {
                    state_ = State::wait;
                }
            }
            break;

        case State::ass_part3:
                                
            if(inputsROB.io_part3_assembled ){
                inputsROB.io_part3_assembled = false;
                //state_ = State::wait;
                cmpt2 ++;
                
                if(cmpt2 == 3){
                    ROS_INFO("[Controller] 3 parts");
                    state_ = State::assemblage;
                }
                else
                {
                    state_ = State::wait;
                }
            }
            break;
                                
        case State::assemblage:
            ROS_INFO("[Controller] assembly");
            outputsASS.AS_start = true;
			sendOuputs_assembly_station(outputsASS);
            cmpt2 = 0;
            state_ = State::wait;

            break;

        }
    }
}

private:

    int cmpt2 = 0;

    enum class State {test,assemblage, wait, wait_at_sc, wait_cam, wait_grasp, decision, wait_at_as, ass_part1, ass_part2, ass_part3, wait_at_ev, rels };

    State state_;
};



int main(int argc, char* argv[]) {
    ros::init(argc, argv, "controller");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Controller controller(node);

    while (ros::ok()) {
        controller.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}

