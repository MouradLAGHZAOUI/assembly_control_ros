#include <ros/ros.h>

#include <assembly_control_ros/evacuation_conveyor_state.h>
#include <assembly_control_ros/evacuation_conveyor_command.h>
#include <assembly_control_ros/evacuation_conveyor_input.h>





#include <common/machine_controller.hpp>

class EvacuationConveyor
    : public MachineController<assembly_control_ros::evacuation_conveyor_state,
                               assembly_control_ros::evacuation_conveyor_input,
                               assembly_control_ros::evacuation_conveyor_command,
                               assembly_control_ros::evacuation_conveyor_input> {
public:
    EvacuationConveyor(ros::NodeHandle node)
        : MachineController(node, "evacuation_conveyor"), state_(State::On) {
    }

    virtual void process() override {
        assembly_control_ros::evacuation_conveyor_command commands;
        assembly_control_ros::evacuation_conveyor_input input;

        auto& inputs = getInputs();

        switch (state_) {

        case State::On:
        commands.on = true;
            if (inputs.stop_EC) {
               inputs.stop_EC = false;
                ROS_INFO("[EvacuationConveyor] Off");
                state_ = State::Off;
            }
            break;

        case State::Off:
            if (inputs.start_EC) {
                inputs.start_EC = false;
                ROS_INFO("[EvacuationConveyor] On");
                state_ = State::On;
            }
            break;
        }

        sendCommands(commands);

    }

private:
    enum class State { On, Off };

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "evacuation_conveyor");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    EvacuationConveyor conveyor(node);

    while (ros::ok()) {
        conveyor.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}
