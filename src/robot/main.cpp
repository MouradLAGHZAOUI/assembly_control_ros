#include <ros/ros.h>

#include <assembly_control_ros/robot_state.h>
#include <assembly_control_ros/robot_command.h>
#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>


#include <common/machine_controller.hpp>

class Robot
    : public MachineController<assembly_control_ros::robot_state,
                               assembly_control_ros::robot_input,
                               assembly_control_ros::robot_command,
                               assembly_control_ros::robot_output> {
public:
    Robot(ros::NodeHandle node)
        : MachineController(node, "robot"), state_(State::Idle) {
    }

    virtual void process() override {
        assembly_control_ros::robot_command commands;
        assembly_control_ros::robot_output outputs;

        auto& inputs = getInputs();
        auto& states = getState();


        switch (state_) {

            case State::Idle:  
            if(inputs.io_grasp && state_ == State::Idle ){

                inputs.io_grasp = false;
                state_ = State::grasp;

            }


            if(inputs.io_release && state_ == State::Idle ){

                inputs.io_release = false;
                state_ = State::release;

            }


            if(inputs.io_assemble_part1 && state_ == State::Idle ){

                inputs.io_assemble_part1 = false;
                state_ = State::assemble_part1;

            }

            if(inputs.io_assemble_part2 && state_ == State::Idle ){

                inputs.io_assemble_part2 = false;
                state_ = State::assemble_part2;

            }

            if(inputs.io_assemble_part3 && state_ == State::Idle ){
                
                inputs.io_assemble_part3 = false;
                state_ = State::assemble_part3;

            }


            if(inputs.io_move_left && state_ == State::Idle && (~getState().at_assembly_station) ){
                inputs.io_move_left = false;
                state_ = State::move_left;

            }


            if(inputs.io_move_right && state_ == State::Idle && (~getState().at_evacuation_conveyor)){
                inputs.io_move_right = false;
                state_ = State::move_right;
            
            }
            break;

            case State::grasp:
                commands.grasp = true;
                sendCommands(commands);

                if(getState().part_grasped){
                    
                    outputs.io_part_grasped = true;
                    sendOuputs(outputs);
                    state_ = State::Idle;
                    ROS_INFO("[robot] grasped");
                }
                break;

            case State::release:
                
                commands.release = true;
                sendCommands(commands);

                if(getState().part_released){

                    ROS_INFO("[robot] released");
                    outputs.io_part_released = true;
                    sendOuputs(outputs);
                    state_ = State::Idle;
                }
                break;

            case State::assemble_part1:

                commands.assemble_part1 = true;
                sendCommands(commands);

                if(getState().part1_assembled){
                    ROS_INFO("[robot] part1_assembled");
                    outputs.io_part1_assembled = true;
                    sendOuputs(outputs);
                    state_ = State::Idle;
                }
                break;

            case State::assemble_part2:

                commands.assemble_part2 = true;
                sendCommands(commands);

                if(getState().part2_assembled){
                    ROS_INFO("[robot] part2_assembled");
                    outputs.io_part2_assembled = true;
                    sendOuputs(outputs);
                    state_ = State::Idle;
                }
                break;

            case State::assemble_part3:

                commands.assemble_part3 = true;
                sendCommands(commands);

                if(getState().part3_assembled){
                    ROS_INFO("[robot] part3_assembled");
                    outputs.io_part3_assembled = true;
                    sendOuputs(outputs);
                    state_ = State::Idle;
                }
                break;

            case State::move_left:
                    ROS_INFO("[robot] move_left");

                    if(SC == true){
                            state_ = State::SC_to_ASS;
                            ROS_INFO("[robot] Robot_go_to_assembly_station");
                    }
                    if(Eva == true){
                            state_ = State::EVA_to_SC;
                            ROS_INFO("[robot] Robot_go_to_supply_conveyor");
                    }

                    break;

            case State::SC_to_ASS:
                    
                commands.move_left = true;
                sendCommands(commands);
                ros::Duration(1).sleep();
                    if(getState().at_assembly_station){
                        ROS_INFO("[robot] robot_at_assembly_station");
                        //p0 = true;
                        ass = true;
                        outputs.io_at_assembly_station = true;
                        sendOuputs(outputs);
                        SC = false;
                        commands.move_left = false;
                        sendCommands(commands);
                        ros::Duration(2).sleep();
                        state_ = State::Idle;
                    }

                break;
                
            case State::EVA_to_SC:

                commands.move_left = true;
                sendCommands(commands);
                ros::Duration(0.5).sleep();

                    if(getState().at_supply_conveyor){
                        ROS_INFO("[robot] robot_at_supply_conveyor");
                        ///p0 = true;
                        SC = true;
                        outputs.io_at_supply_conveyor = true;
                        sendOuputs(outputs);
                        Eva = false;
                            
                        commands.move_left = false;
                        sendCommands(commands);
                        ros::Duration().sleep();
                        state_ = State::Idle;
                    }
                break;

            case State::move_right:
                ROS_INFO("[robot] move_right");
                    if(SC){
                        state_ = State::SC_to_EVA;
                        ROS_INFO("[robot] Robot_go_to_evacuation_conveyor");
                    }
                    if(ass){
                        state_ = State::ASS_to_SC;
                        ROS_INFO("[robot] Robot_go_to_supply_conveyor");
                    }

                break;
            

            case State::SC_to_EVA:

                commands.move_right = true;
                sendCommands(commands);
                ros::Duration(1).sleep();

                if(getState().at_evacuation_conveyor ){
                    ROS_INFO("[robot] Robot_at_evacuation_conveyor");
                    //p1 = true;
                    Eva = true;
                    outputs.io_at_evacuation_conveyor = true;
                    sendOuputs(outputs);
                    SC = false;
                    state_ = State::Idle;
                    commands.move_right = false;
                    sendCommands(commands);
                    ros::Duration(2).sleep();
                } 
                break;
                        
            case State::ASS_to_SC: 
                commands.move_right = true;
                sendCommands(commands);
                ros::Duration().sleep();
                
                if(getState().at_supply_conveyor){
                    ROS_INFO("[robot] Robot_at_supply_conveyor");
                    //p1 = true;
                    SC = true;
                    outputs.io_at_supply_conveyor = true;
                    sendOuputs(outputs);
                    ass = false;
                    state_ = State::Idle;
                    commands.move_right = false;
                    sendCommands(commands);
                    ros::Duration(2).sleep();
                }                     


                break;


        }

        sendCommands(commands);


    }

private:

        bool ass = true;
        bool SC = false;
        bool Eva = false;

    enum class State {Init, ASS_to_SC, SC_to_EVA, SC_to_ASS,EVA_to_SC, Idle, grasp, release, assemble_part1, assemble_part2, assemble_part3, move_left, move_right };

    State state_;
};


int main(int argc, char* argv[]) {
    ros::init(argc, argv, "robot");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Robot Rob(node);

    while (ros::ok()) {
        Rob.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}
